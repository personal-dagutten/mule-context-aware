package mulecontextaware;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.jcl.LogFactoryImpl;
import org.mule.api.MuleContext;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleException;
import org.mule.api.context.MuleContextAware;
import org.mule.api.lifecycle.Callable;
import org.mule.api.lifecycle.Stoppable;

public class ClassStopLc implements Stoppable,MuleContextAware,Callable {

	@Override
	public void stop() throws MuleException {
		logger.info(" ClassStopLC Stopping");
	}

	private MuleContext context;
	private final static Logger logger = LogManager.getLogger(ClassStopLc.class);
	
	
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		context.stop();
		context.dispose();
		return null;
	}

	@Override
	public void setMuleContext(MuleContext context) {
		this.context = context;
		
	}
}
