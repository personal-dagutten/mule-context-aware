package mulecontextaware;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mule.api.MuleContext;
import org.mule.api.context.MuleContextAware;
import org.mule.api.context.notification.MuleContextNotificationListener;
import org.mule.context.notification.MuleContextNotification;

public class MuleContextListener implements MuleContextNotificationListener<MuleContextNotification>,MuleContextAware {

	private MuleContext context;
	private final static Logger logger = LogManager.getLogger(MuleContextListener.class);
	
	@Override
	public void onNotification(MuleContextNotification notification) {
		logger.info("NOTIFICATION RECEIVED!!!!!");
		logger.info(MuleContextNotification.getActionName(notification.getAction()));
		if(notification.getAction() == MuleContextNotification.CONTEXT_STOPPING ){
			//replace with your tcp call 
			logger.info(" MY ACTION HAS BEEN TRIGGER");
		}
	}

	@Override
	public void setMuleContext(MuleContext context) {
		this.context = context;
	}
}
